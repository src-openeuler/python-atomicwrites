%global _description\
This Python module provides atomic file writes on POSIX operating systems.\
It sports:\
* Race-free assertion that the target file doesn't yet exist\
* Windows support\
* Simple high-level API that wraps a very flexible class-based API\
* Consistent error handling across platforms.

Name:           python-atomicwrites
Version:        1.4.1
Release:        2
Summary:        Python Atomic file writes on POSIX
License:        MIT
URL:            https://github.com/untitaker/%{name}
Source0:        https://github.com/untitaker/%{name}/archive/refs/tags/%{version}.tar.gz
# https://github.com/sphinx-doc/sphinx/blob/master/doc/usage/extensions/intersphinx.rst
Patch0:         Adapt-to-intersphinx_mapping-format-due-to-Sphinx-upgrade.patch
BuildArch:      noarch

BuildRequires:  python3-devel python3-setuptools python3-sphinx python3-pytest

%description %_description

%package -n python3-atomicwrites
Summary:        %summary

%description -n python3-atomicwrites %_description

%package -n python-atomicwrites-help
Summary:        %summary

%description -n python-atomicwrites-help
This package conatins manual pages for python3-atomicwrites

%prep
%autosetup -n %{name}-%{version} -p1

%build
%py3_build
export PYTHONPATH=`pwd`
cd docs
%make_build SPHINXBUILD=sphinx-build-3 man
cd ..
unset PYTHONPATH

%install
%py3_install

install -d "$RPM_BUILD_ROOT%{_mandir}/man1"
cp -r docs/_build/man/*.1 "$RPM_BUILD_ROOT%{_mandir}/man1"

%check
%{__python3} -m pytest -v

%files -n python3-atomicwrites
%doc README.rst LICENSE
%{python3_sitelib}/*

%files -n python-atomicwrites-help
%{_mandir}/man1/atomicwrites.1.*

%changelog
* Fri Dec 20 2024 wangkai <13474090681@163.com> - 1.4.1-2
- Fix build failure due to Sphinx upgrade to 8.1.3

* Thu  Dec 08 2022 wubijie <wubijie@kylinos.cn> - 1.4.1-1
- Update package to version 1.4.1

* Mon Jun 06 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 1.4.0-1
- Update to 1.4.0

* Sat Aug 08 2020 lingsheng <lingsheng@huawei.com> - 1.1.5-14
- Remove python2-atomicwrites subpackage

* Fri Nov 15 2019 sunguoshuai <sunguoshuai@huawei.com> - 1.1.5-13
- Package init
